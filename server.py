from flask import Flask, request
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from json import dumps
from flask_jsonpify import jsonify


db_connect = create_engine('sqlite:///chinook.db')
app = Flask(__name__)
api = Api(app)


class Employees(Resource):
    def get(self):
        conn = db_connect.connect()  # conect to db
        query = conn.execute('SELECT * FROM employees') # execute the query
        return {'employees': [i[0] for i in query.cursor.fetchall()]}


class Tracks(Resource):
    def get(self):
        conn = db_connect.connect()
        query = conn.execute(
                "select trackid, name, composer, unitprice from tracks;")
        result = {
                'data': [dict(zip(tuple (query.keys())
                    ,i)) for i in query.cursor]}
        return jsonify(result)


class Employees_Name(Resource):
    def get(self, employee_id):
        conn = db_connect.connect()  # conect to db
        query = conn.execute(
                'SELECT * FROM employees WHERE EmployeeID =%d'
                %int(employee_id)) # execute the query
        result = {'data': [dict(zip(tuple(query.keys()), i ))
            for i in query.cursor]}
        return jsonify(result)

api.add_resource(Employees, '/employees') # Route_1
api.add_resource(Tracks, '/tracks') # Route_2
api.add_resource(Employees_Name, '/employees/<employee_id>') # Route_3

"""There will be three routes created :

    http://127.0.0.1:5002/employees shows ids of all the employees in database
    http://127.0.0.1:5002/tracks shows tracks details
    http://127.0.0.1:5002/employees/8 shows details of employee whose employeeid is 8"""

if __name__ == '__main__':
     app.run(port='5002')
