#  RestApi

REST as a set of conventions taking advantage of the HTTP protocol to provide CRUD (Create, Read, Update, and Delete) behavior on things and collections of those things.

* Practice for the creation of RestApi according to https://www.codementor.io/sagaragarwal94/building-a-basic-restful-api-in-python-58k02xsiq
* the RestApi's methods for odoo [Here](https://www.odoo.com/apps/modules/11.0/restapi/)
* DB sample [Here](http://www.sqlitetutorial.net/sqlite-sample-database/)

### Swagger

Provides a lot of functionality: validation of input and output data to and from your API, an easy way to configure the API URL endpoints and the parameters expected, and a really nice UI interface to work with the created API and explore it.

### Interesting Links

* [Swagger](https://swagger.io/)
* [Python3 implementation RealPython](https://realpython.com/flask-connexion-rest-api/)


